# Shared Expenses App

Esta aplicación permite a un grupo de amigos llevar un registro de sus gastos compartidos. La aplicación cuenta con dos componentes principales:

- Expenses: muestra una lista de los gastos realizados por el grupo de amigos.

La arquitectura utilizada en la aplicación sigue una estructura de 3 capas, con los siguientes componentes:

- Components: los componentes de Angular que manejan la interfaz de usuario.
- Services: servicios que manejan la lógica de negocio de la aplicación.
- Repositories: interfaces que especifican las operaciones que se pueden realizar con los datos, en este caso los gastos compartidos.

## Objetivos

El objetivo de esta aplicación es servir como un ejemplo básico de una aplicación Angular con arquitectura de 3 capas. Debido a su naturaleza de demostración, los objetivos logrados son pocos, pero se espera que la estructura sirva como punto de partida para proyectos más grandes y complejos.

## Testeo

Se han realizado pruebas unitarias básicas utilizando el framework de pruebas de Angular.

## Despliegue

Para desplegar la aplicación, siga los siguientes pasos:

- Descargue o clone el repositorio en su ordenador.
- Abra una terminal en la carpeta raíz del proyecto.
- Ejecute el comando npm install para instalar las dependencias necesarias.
- Ejecute el comando ng serve para iniciar la aplicación.
- Abra su navegador web y vaya a http://localhost:4200 para ver la aplicación en funcionamiento.

## Docker

Si lo deseas, puedes ejecutar la aplicación dentro de un contenedor Docker. Para ello, sigue los siguientes pasos:

1. Abre una terminal en la carpeta raíz del proyecto.
2. Ejecuta el siguiente comando para construir la imagen de Docker:

```docker
   docker build -t gastos-compartidos-app .
```

3. Una vez finalizado el proceso de construcción, ejecuta el siguiente comando para iniciar el contenedor:

```docker
docker run -p 8080:80 gastos-compartidos-app
```

4. Abre tu navegador y accede a http://localhost:8080 para ver la aplicación en funcionamiento.

Con estos pasos, tendrás la aplicación funcionando dentro de un contenedor Docker.
