import {
  FRIENDS_REPOSITORY_TOKEN,
  FriendsRepositoryImpl,
} from './core/repositories/friends.repository';
import {
  ExpensesRepositoryImpl,
  EXPENSES_REPOSITORY_TOKEN,
} from './core/repositories/expenses.repository';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SharedExpensesComponent } from './shared-expenses/shared-expenses.component';
import { AddPaymentComponent } from './add-payment/add-payment.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SharedExpensesComponent,
    AddPaymentComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule],
  providers: [
    {
      provide: EXPENSES_REPOSITORY_TOKEN,
      useClass: ExpensesRepositoryImpl,
    },
    {
      provide: FRIENDS_REPOSITORY_TOKEN,
      useClass: FriendsRepositoryImpl,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
