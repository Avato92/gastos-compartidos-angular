import { FriendsService } from './../core/services/friends.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedExpensesService } from '../core/services/shared-expenses.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Friend } from '../core/models/friend.model';

@Component({
  selector: 'app-add-payment',
  templateUrl: './add-payment.component.html',
  //styleUrls: ['./add-payment.component.css']
})
export class AddPaymentComponent implements OnInit {
  friends: Friend[] = [];
  addExpense: FormGroup;

  constructor(
    private expensesService: SharedExpensesService,
    private friendsService: FriendsService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.addExpense = this.formBuilder.group({
      description: '',
      amount: '',
      friendId: 0,
      date: new Date(),
    });
  }

  ngOnInit(): void {
    this.friendsService.getFriends().subscribe((friends: Friend[]) => {
      this.friends = friends;
    });
  }

  onSubmit(): void {
    const expense = {
      description: this.addExpense.value.description,
      amount: this.addExpense.value.amount,
      friendId: this.addExpense.value.friendId,
      date: this.addExpense.value.date,
    };
    this.expensesService.addExpense(expense);
    this.addExpense.reset();
    this.router.navigate(['/']);
  }
}
