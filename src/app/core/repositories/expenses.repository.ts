import { Injectable, InjectionToken } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Expense } from '../models/expense.model';
import { Friend } from '../models/friend.model';
import { expenses, friends } from './mockData';

export const EXPENSES_REPOSITORY_TOKEN = new InjectionToken<ExpensesRepository>(
  'ExpensesRepository'
);

export interface ExpensesRepository {
  getExpenses(): Observable<Expense[]>;
  addExpense(expense: Expense): Observable<Expense>;
}

@Injectable({
  providedIn: 'root',
})
export class ExpensesRepositoryImpl implements ExpensesRepository {
  private expenses: Expense[] = expenses;

  private friends: Friend[] = friends;

  getExpenses(): Observable<Expense[]> {
    const sortedExpenses = this.expenses.sort(
      (a, b) => b.date.getTime() - a.date.getTime()
    );
    return of(sortedExpenses);
  }

  addExpense(expense: Expense): Observable<Expense> {
    expense.id = this.expenses.length + 1;
    this.expenses.push(expense);
    return of(expense);
  }
}
