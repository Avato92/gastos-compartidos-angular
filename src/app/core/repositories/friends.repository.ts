import { Injectable, InjectionToken } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap, map } from 'rxjs/operators';
import { Friend } from '../models/friend.model';
import { friends } from './mockData';

export const FRIENDS_REPOSITORY_TOKEN = new InjectionToken<FriendsRepository>(
  'FriendsRepository'
);

export interface FriendsRepository {
  getFriends(): Observable<Friend[]>;
  addFriend(name: string): Observable<Friend>;
  getFriendName(id: number): Observable<string | undefined>;
}

@Injectable({
  providedIn: 'root',
})
export class FriendsRepositoryImpl implements FriendsRepository {
  private friends: Friend[] = friends;

  getFriends(): Observable<Friend[]> {
    return of(this.friends);
  }

  addFriend(name: string): Observable<Friend> {
    const friend: Friend = {
      id: this.friends.length + 1,
      name: name,
    };
    this.friends.push(friend);
    return of(friend);
  }

  getFriendName(friendId: number): Observable<string | undefined> {
    return this.getFriends().pipe(
      switchMap((friends: Friend[]) =>
        of(friends.find((friend: Friend) => friend.id === friendId))
      ),
      map((friend: Friend | undefined) => friend?.name)
    );
  }
}
