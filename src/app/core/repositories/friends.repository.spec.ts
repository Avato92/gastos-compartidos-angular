import { TestBed } from '@angular/core/testing';
import { Friend } from '../models/friend.model';
import { FriendsRepositoryImpl } from './friends.repository';
import { friends } from './mockData';

describe('FriendsRepositoryImpl', () => {
  let repository: FriendsRepositoryImpl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FriendsRepositoryImpl],
    });
    repository = TestBed.inject(FriendsRepositoryImpl);
  });

  it('should be created', () => {
    expect(repository).toBeTruthy();
  });

  describe('addFriend', () => {
    it('should add a friend to the list', (done) => {
      const name = 'Test Friend';
      repository.addFriend(name).subscribe((friend: Friend) => {
        expect(friend.id + 1).toBe(friends.length + 1);
        expect(friend.name).toBe(name);
        done();
      });
    });
  });

  describe('getFriendName', () => {
    it('should return the name of the friend with the given id', (done) => {
      const friendId = 1;
      repository.getFriendName(friendId).subscribe((name) => {
        expect(name).toBe(friends[friendId - 1].name);
        done();
      });
    });

    it('should return undefined if no friend is found with the given id', (done) => {
      const friendId = 999;
      repository.getFriendName(friendId).subscribe((name) => {
        expect(name).toBeUndefined();
        done();
      });
    });
  });
});
