import { ExpensesRepositoryImpl } from './expenses.repository';
import { Expense } from '../models/expense.model';

describe('ExpensesRepositoryImpl', () => {
  let service: ExpensesRepositoryImpl;

  beforeEach(() => {
    service = new ExpensesRepositoryImpl();
  });

  it('should get expenses in descending order by date', () => {
    const expenses: Expense[] = [
      {
        id: 1,
        friendId: 1,
        amount: 100,
        description: 'Dinner',
        date: new Date('2022-03-09T14:00:00.000Z'),
      },
      {
        id: 2,
        friendId: 2,
        amount: 50,
        description: 'Movie tickets',
        date: new Date('2022-03-08T20:00:00.000Z'),
      },
    ];

    service.getExpenses().subscribe((sortedExpenses) => {
      expect(sortedExpenses).toEqual(expenses);
    });
  });

  it('should add an expense to the expenses list', () => {
    const expense: Expense = {
      id: 3,
      friendId: 1,
      amount: 75,
      description: 'Lunch',
      date: new Date(),
    };

    service.addExpense(expense).subscribe((addedExpense) => {
      expect(addedExpense).toEqual(expense);
      expect(service['expenses']).toContain(addedExpense);
    });
  });
});
