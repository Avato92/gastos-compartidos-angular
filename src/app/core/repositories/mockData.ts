import { Expense } from '../models/expense.model';
import { Friend } from '../models/friend.model';

export const expenses: Expense[] = [
  {
    id: 1,
    friendId: 1,
    amount: 100,
    description: 'Dinner',
    date: new Date('2022-03-09T14:00:00.000Z'),
  },
  {
    id: 2,
    friendId: 2,
    amount: 50,
    description: 'Movie tickets',
    date: new Date('2022-03-08T20:00:00.000Z'),
  },
];

export const friends: Friend[] = [
  {
    id: 1,
    name: 'John',
  },
  {
    id: 2,
    name: 'Jane',
  },
];
