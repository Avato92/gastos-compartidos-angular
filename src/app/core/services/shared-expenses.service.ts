import { Injectable, Inject } from '@angular/core';
import { Observable, switchMap } from 'rxjs';
import { map } from 'rxjs/operators';
import { Expense } from '../models/expense.model';
import {
  ExpensesRepository,
  EXPENSES_REPOSITORY_TOKEN,
} from '../repositories/expenses.repository';
import { FriendsService } from './friends.service';

@Injectable({
  providedIn: 'root',
})
export class SharedExpensesService {
  constructor(
    @Inject(EXPENSES_REPOSITORY_TOKEN)
    private expensesRepository: ExpensesRepository,
    private friendsService: FriendsService
  ) {}

  getExpenses(): Observable<Expense[]> {
    return this.expensesRepository.getExpenses();
  }

  addExpense(newExpense: Omit<Expense, 'id'>): Observable<Expense> {
    const expenses$: Observable<Expense[]> = this.getExpenses();

    return expenses$.pipe(
      switchMap((expenses: Expense[]) => {
        const length = expenses.length;
        const expense: Expense = {
          id: length + 1,
          ...newExpense,
        };
        return this.expensesRepository.addExpense(expense);
      })
    );
  }

  getBalance(): Observable<Map<string, number>> {
    return this.getExpenses().pipe(
      map((expenses: Expense[]) => {
        const balanceMap = new Map<string, number>();
        expenses.forEach((expense: Expense) => {
          const friendName = this.friendsService.getFriendName(
            expense.friendId
          );
          if (friendName) {
            const currentBalance = balanceMap.get(friendName) || 0;
            balanceMap.set(friendName, currentBalance + expense.amount);
          }
        });
        return balanceMap;
      })
    );
  }
}
