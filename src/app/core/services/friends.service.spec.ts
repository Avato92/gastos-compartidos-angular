import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Friend } from '../models/friend.model';
import { FriendsRepository } from '../repositories/friends.repository';
import { FRIENDS_REPOSITORY_TOKEN } from '../repositories/friends.repository';
import { FriendsService } from './friends.service';

describe('FriendsService', () => {
  let service: FriendsService;
  let repository: jasmine.SpyObj<FriendsRepository>;

  beforeEach(() => {
    const spy = jasmine.createSpyObj('FriendsRepository', [
      'addFriend',
      'getFriendName',
    ]);

    TestBed.configureTestingModule({
      providers: [
        FriendsService,
        { provide: FRIENDS_REPOSITORY_TOKEN, useValue: spy },
      ],
    });

    service = TestBed.inject(FriendsService);
    repository = TestBed.inject(
      FRIENDS_REPOSITORY_TOKEN
    ) as jasmine.SpyObj<FriendsRepository>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('addFriend', () => {
    it('should call the repository to add a new friend', () => {
      const name = 'John';
      const newFriend = { id: 1, name } as Friend;
      repository.addFriend.and.returnValue(of(newFriend));

      service.addFriend(name).subscribe((friend) => {
        expect(friend).toEqual(newFriend);
        expect(repository.addFriend).toHaveBeenCalledWith(name);
      });
    });
  });

  describe('getFriendName', () => {
    it('should call the repository to get the name of a friend and return the name', () => {
      const id = 1;
      const friendName = 'John';
      repository.getFriendName.and.returnValue(of(friendName));

      const name = service.getFriendName(id);
      expect(repository.getFriendName).toHaveBeenCalledWith(id);
      expect(name).toEqual(friendName);
    });
  });
});
