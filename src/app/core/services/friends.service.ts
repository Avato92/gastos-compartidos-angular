import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Friend } from '../models/friend.model';
import {
  FriendsRepository,
  FRIENDS_REPOSITORY_TOKEN,
} from '../repositories/friends.repository';

@Injectable({
  providedIn: 'root',
})
export class FriendsService {
  constructor(
    @Inject(FRIENDS_REPOSITORY_TOKEN)
    private friendsRepository: FriendsRepository
  ) {}

  addFriend(name: string): Observable<Friend> {
    return this.friendsRepository.addFriend(name);
  }

  getFriends(): Observable<Friend[]> {
    return this.friendsRepository.getFriends();
  }

  getFriendName(id: number): string | undefined {
    let friendName: string | undefined;
    this.friendsRepository.getFriendName(id).subscribe((friend?: string) => {
      friendName = friend;
    });
    return friendName;
  }
}
