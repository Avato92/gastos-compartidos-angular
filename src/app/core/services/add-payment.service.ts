import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Expense } from '../models/expense.model';
import {
  EXPENSES_REPOSITORY_TOKEN,
  ExpensesRepository,
} from '../repositories/expenses.repository';

@Injectable({
  providedIn: 'root',
})
export class AddPaymentService {
  constructor(
    @Inject(EXPENSES_REPOSITORY_TOKEN)
    private expensesRepository: ExpensesRepository
  ) {}

  addExpense(expense: Expense): Observable<Expense> {
    return this.expensesRepository.addExpense(expense);
  }
}
