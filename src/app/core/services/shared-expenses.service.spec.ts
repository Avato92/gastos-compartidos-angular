import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Expense } from '../models/expense.model';
import {
  ExpensesRepository,
  EXPENSES_REPOSITORY_TOKEN,
} from '../repositories/expenses.repository';
import { SharedExpensesService } from './shared-expenses.service';
import { FriendsService } from './friends.service';

describe('SharedExpensesService', () => {
  let service: SharedExpensesService;
  let expensesRepositorySpy: jasmine.SpyObj<ExpensesRepository>;
  let friendsServiceSpy: jasmine.SpyObj<FriendsService>;

  beforeEach(() => {
    const expensesSpy = jasmine.createSpyObj('ExpensesRepository', [
      'getExpenses',
      'addExpense',
    ]);
    const friendsSpy = jasmine.createSpyObj('FriendsService', [
      'getFriendName',
    ]);

    TestBed.configureTestingModule({
      providers: [
        SharedExpensesService,
        { provide: EXPENSES_REPOSITORY_TOKEN, useValue: expensesSpy },
        { provide: FriendsService, useValue: friendsSpy },
      ],
    });
    service = TestBed.inject(SharedExpensesService);
    expensesRepositorySpy = TestBed.inject(
      EXPENSES_REPOSITORY_TOKEN
    ) as jasmine.SpyObj<ExpensesRepository>;
    friendsServiceSpy = TestBed.inject(
      FriendsService
    ) as jasmine.SpyObj<FriendsService>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getExpenses', () => {
    it('should return expenses', () => {
      const expenses: Expense[] = [
        {
          id: 1,
          friendId: 1,
          amount: 100,
          description: 'Test expense 1',
          date: new Date(),
        },
        {
          id: 2,
          friendId: 2,
          amount: 50,
          description: 'Test expense 2',
          date: new Date(),
        },
      ];
      expensesRepositorySpy.getExpenses.and.returnValue(of(expenses));
      service.getExpenses().subscribe((result) => {
        expect(result).toEqual(expenses);
      });
    });
  });

  describe('addExpense', () => {
    it('should add an expense', () => {
      const expense: Omit<Expense, 'id'> = {
        friendId: 1,
        amount: 100,
        description: 'Test expense',
        date: new Date(),
      };
      const expected = { ...expense, id: 1 };
      expensesRepositorySpy.getExpenses.and.returnValue(of([]));
      expensesRepositorySpy.addExpense.and.returnValue(of(expected));
      service.addExpense(expense).subscribe((result) => {
        expect(result).toEqual(expected);
      });
    });
  });

  describe('getBalance', () => {
    it('should return an empty balance map when there are no expenses', () => {
      const expenses: Expense[] = [];
      expensesRepositorySpy.getExpenses.and.returnValue(of(expenses));

      const expectedBalanceMap = new Map<string, number>();

      service.getBalance().subscribe((balanceMap) => {
        expect(balanceMap.size).toBe(expectedBalanceMap.size);
      });
    });
  });
});
