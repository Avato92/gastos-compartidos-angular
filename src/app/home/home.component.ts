import { Component } from '@angular/core';
import { Expense } from '../core/models/expense.model';
import { SharedExpensesService } from '../core/services/shared-expenses.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  //styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  expenses: Expense[] = [];

  constructor(private expensesService: SharedExpensesService) {}
}
