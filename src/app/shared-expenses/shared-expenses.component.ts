import { FriendsService } from './../core/services/friends.service';
import { Component, Input } from '@angular/core';
import { SharedExpensesService } from '../core/services/shared-expenses.service';
import { Expense } from '../core/models/expense.model';

@Component({
  selector: 'app-shared-expenses',
  templateUrl: './shared-expenses.component.html',
})
export class SharedExpensesComponent {
  @Input() expenses: Expense[] = [];

  constructor(
    private sharedExpensesService: SharedExpensesService,
    private friendsService: FriendsService
  ) {}

  ngOnInit(): void {
    this.sharedExpensesService
      .getExpenses()
      .subscribe((expenses: Expense[]) => {
        this.expenses = expenses;
      });
  }

  getFriendName(friendId: number): string | undefined {
    return this.friendsService.getFriendName(friendId);
  }
}
